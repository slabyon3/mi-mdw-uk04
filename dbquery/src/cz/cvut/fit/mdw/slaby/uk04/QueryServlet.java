package cz.cvut.fit.mdw.slaby.uk04;

import weblogic.jdbc.common.internal.RmiDataSource;
import weblogic.jdbc.jta.DataSource;
import weblogic.servlet.annotation.WLServlet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Hashtable;

@WLServlet(name = "Query Servlet", mapping = "query")
public class QueryServlet extends HttpServlet {
    private static final String[] headers = {"ID", "Type", "Location", "Capacity", "Occupied", "Trip", "Person"};

    private int[] fieldSizes = new int[headers.length];

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Hashtable ht = new Hashtable();
        ht.put(Context.INITIAL_CONTEXT_FACTORY,
                "weblogic.jndi.WLInitialContextFactory");
        ht.put(Context.PROVIDER_URL,
                "t3://localhost:7001");
        Context context;
        RmiDataSource dataSource;

        try {
            context = new InitialContext(ht);
            dataSource = (RmiDataSource) context.lookup("mimdw-hw04-db");
        } catch (NamingException e) {
            e.printStackTrace();
            return;
        }

        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT id,type,location,capacity,occupied,trip,person FROM records;");
            printResults(results, response.getWriter());
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    private void printResults(ResultSet results, PrintWriter writer) throws SQLException {
        updateFieldSizes(results);

        if (!results.first())
            return;
        writeSeparatorRow(writer);
        writeValueRow(headers, writer);
        writeSeparatorRow(writer);
        do {
            String[] values = {
                    String.valueOf(results.getInt("id")),
                    String.valueOf(results.getString("type")),
                    String.valueOf(results.getString("location")),
                    String.valueOf(results.getInt("capacity")),
                    String.valueOf(results.getInt("occupied")),
                    String.valueOf(results.getInt("trip")),
                    String.valueOf(results.getString("person"))
            };
            writeValueRow(values, writer);
        } while (results.next());
        writeSeparatorRow(writer);
    }

    private void writeValueRow(String[] values, PrintWriter writer) {
        for (int i = 0; i < values.length; i++) {
            writer.print("| ");
            writer.print(values[i]);
            for (int j = values[i].length(); j < fieldSizes[i]; j++)
                writer.print(" ");
            writer.print(" ");
        }
        writer.println("|");
    }

    private void writeSeparatorRow(PrintWriter writer) {
        for (int size : fieldSizes) {
            writer.print("+");
            for (int i = 0; i < size + 2; i++)
                writer.print("-");
        }
        writer.println("+");
    }

    private void updateFieldSizes(ResultSet results) throws SQLException {
        for (int i = 0; i < fieldSizes.length; i++)
            fieldSizes[i] = headers[i].length();
        if (!results.first())
            return;
        do {
            fieldSizes[0] = Math.max(fieldSizes[0], String.valueOf(results.getInt("id")).length());
            fieldSizes[1] = Math.max(fieldSizes[1], String.valueOf(results.getString("type")).length());
            fieldSizes[2] = Math.max(fieldSizes[2], String.valueOf(results.getString("location")).length());
            fieldSizes[3] = Math.max(fieldSizes[3], String.valueOf(results.getInt("capacity")).length());
            fieldSizes[4] = Math.max(fieldSizes[4], String.valueOf(results.getInt("occupied")).length());
            fieldSizes[5] = Math.max(fieldSizes[5], String.valueOf(results.getInt("trip")).length());
            fieldSizes[6] = Math.max(fieldSizes[6], String.valueOf(results.getString("person")).length());
        } while (results.next());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
