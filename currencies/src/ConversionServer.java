import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class ConversionServer extends UnicastRemoteObject implements CurrencyConverter {
    private Map<CurrencyPair, Double> rateMap;

    protected ConversionServer() throws RemoteException {
        rateMap = new HashMap<>();

        rateMap.put(new CurrencyPair(Currency.USD, Currency.USD), 1.0);
        rateMap.put(new CurrencyPair(Currency.EUR, Currency.EUR), 1.0);
        rateMap.put(new CurrencyPair(Currency.GBP, Currency.GBP), 1.0);
        rateMap.put(new CurrencyPair(Currency.USD, Currency.EUR), 0.86);
        rateMap.put(new CurrencyPair(Currency.EUR, Currency.USD), 1.16);
        rateMap.put(new CurrencyPair(Currency.GBP, Currency.USD), 1.3);
        rateMap.put(new CurrencyPair(Currency.USD, Currency.GBP), 0.76);
        rateMap.put(new CurrencyPair(Currency.GBP, Currency.EUR), 1.13);
        rateMap.put(new CurrencyPair(Currency.EUR, Currency.GBP), 0.88);
    }

    @Override
    public double convert(Currency from, Currency to, double amount) throws RemoteException{
        return amount * rateMap.get(new CurrencyPair(from, to));
    }
}
