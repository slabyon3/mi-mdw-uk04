import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Server {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);

            ConversionServer converter = new ConversionServer();

            Naming.rebind("//0.0.0.0/converter", converter);

            System.out.println("Server started...");
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
