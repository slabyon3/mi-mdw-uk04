import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        System.out.print("Connecting to server...");
        CurrencyConverter converter = null;

        try {
            converter = (CurrencyConverter) Naming.lookup("//0.0.0.0/converter");
        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            e.printStackTrace();
        }

        if (converter == null) {
            System.out.println();
            System.out.println("Failed to connect to the server! Exiting...");
            return;
        }
        System.out.println("Connected");

        Scanner scanner = new Scanner(System.in);
        String from, to;
        double amount;

        Currency cFrom, cTo;

        do {
            System.out.println("Enter two currencies and an amount to convert, or 'exit' to quit:");
            from = scanner.next();
            if (from.equals("exit"))
                break;

            cFrom = checkCurrency(from);
            if (cFrom == null)
                continue;

            to = scanner.next();
            cTo = checkCurrency(to);
            if (cTo == null)
                continue;

            if (!scanner.hasNextDouble()) {
                System.out.println("You must enter an amount to convert!");
                continue;
            }
            amount = scanner.nextDouble();

            double result;
            try {
                result = converter.convert(cFrom, cTo, amount);
            } catch (RemoteException e) {
                System.out.println("Conversion failed!");
                continue;
            }

            System.out.println(amount + " " + cFrom.name() + " = " + result + " " + cTo.name());
        } while (true);
    }

    private static Currency checkCurrency(String name) {
        String currency = name.toUpperCase();
        try {
            return Currency.valueOf(currency);
        } catch (IllegalArgumentException e) {
            System.out.println("There is no currency named: " + name);
            return null;
        }
    }
}
